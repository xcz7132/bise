//导入request.js请求工具
import request from "@/utils/request"
//提供调用注册接口的函数

//添加订单
export const tradeAddService = (tradeDate) => {
    return request.post('/trade', tradeDate)
}

//编辑订单接口
export const tradeUpdateService = (tradeDate) => {
    return request.put('/trade', tradeDate)
}

//删除订单接口
export const tradeDleteService = (id) => {
    return request.delete('/trade?id=' + id)
}

//订单列表查询
export const tradeService = (params) => {
    return request.get('/trade', { params: params })
}
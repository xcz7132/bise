import request from '@/utils/request.js'
import { useTokenStore } from '@/stores/token.js'
//文章分类列表查询
export const articleCategoryListService = () => {
    //const tokenStore = useTokenStore();
    //在pinia中定义的响应式数据，都不需要.value
    //return request.get('/category', { headers: { 'Authorization': tokenStore.token } })
    return request.get('/category')

}

//新增文章分类接口
export const articleCategoryAddService = (categoryDate) => {
    return request.post('/category', categoryDate)
}

//编辑分类接口
export const articleCategoryUpdateService = (categoryDate) => {
    return request.put('/category', categoryDate)
}

//删除分类接口
export const articleCategoryDleteService = (id) => {
    return request.delete('/category?id=' + id)
}

//文章列表查询
export const articleListService = (params) => {
    return request.get('/article', { params: params })
}

//文章添加
export const articleAddService = (articleDate) => {
    return request.post('/article', articleDate)
}
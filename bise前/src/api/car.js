//导入request.js请求工具
import request from "@/utils/request"
//提供调用注册接口的函数

//添加车辆
export const carAddService = (carDate) => {
    return request.post('/car', carDate)
}

//编辑车辆接口
export const carUpdateService = (carDate) => {
    return request.put('/car', carDate)
}

//删除车辆接口
export const carDleteService = (id) => {
    return request.delete('/car?id=' + id)
}

//车辆列表查询
export const carService = (params) => {
    return request.get('/car', { params: params })
}

//获取所有车辆信息
export const carListService = () => {
    return request.get('/car/list')
}
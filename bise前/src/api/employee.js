//导入request.js请求工具
import request from "@/utils/request"
//提供调用注册接口的函数
//员工列表查询
export const employeeServiceList = (params) => {
    return request.get('/employee/list', { params: params })
}

export const employeeService = () => {
    return request.get('/employee')
}
//导入request.js请求工具
import request from "@/utils/request"
//提供调用注册接口的函数

//添加售后
export const afterSalesAddService = (afterSalesDate) => {
    return request.post('/afterSales', afterSalesDate)
}

//编辑售后接口
export const afterSalesUpdateService = (afterSalesDate) => {
    return request.put('/afterSales', afterSalesDate)
}

//删除售后接口
export const afterSalesDleteService = (id) => {
    return request.delete('/afterSales?id=' + id)
}

//售后列表查询
export const afterSalesService = (params) => {
    return request.get('/afterSales', { params: params })
}
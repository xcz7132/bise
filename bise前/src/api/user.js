//导入request.js请求工具
import request from "@/utils/request"
//提供调用注册接口的函数
export const userRegisterService = (registerDate) => {
    //借助于UrlSearchParams完成传递
    const params = new URLSearchParams();
    for (let key in registerDate) {
        params.append(key, registerDate[key]);
    }
    return request.post('/user/register', params);
}

//提供调用登录接口的函数
export const userLoginService = (loginDate) => {
    //借助于UrlSearchParams完成传递
    const params = new URLSearchParams();
    for (let key in loginDate) {
        params.append(key, loginDate[key]);
    }
    return request.post('/user/login', params);
}

//获取用户详细信息
export const userInfoService = () => {
    return request.get('/user/userInfo')
}

//修改个人信息
export const userInfoUpdateService = (userInfoDate) => {
    return request.put('/user/update', userInfoDate)
}
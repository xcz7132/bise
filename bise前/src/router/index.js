import { createRouter, createWebHistory } from "vue-router"

//导入组件
import LoginVue from "@/views/Login.vue"
import LayoutVue from "@/views/Layout.vue"
import UserInfo from '@/views/user/UserInfo.vue'
import UserResetPassword from '@/views/user/UserResetPassword.vue'
import AfterSalesCategory from '@/views/layout/AfterSalesCategory.vue'
import CarManage from "@/views/layout/CarManage.vue"
import TradeManage from "@/views/layout/TradeManage.vue"
import Employees from '@/views/layout/Employees.vue'
//定义路由关系
const routes = [
    { path: '/login', component: LoginVue },
    {
        path: '/',
        component: LayoutVue,
        redirect: '/login',
        children: [
            { path: '/layout/car', component: CarManage },
            { path: '/layout/trade', component: TradeManage },
            { path: '/layout/afterSales', component: AfterSalesCategory },
            { path: '/layout/employee', component: Employees },
            { path: '/user/info', component: UserInfo },
            { path: '/user/resetPassword', component: UserResetPassword }
        ]
    }
]

//创建路由器
const router = createRouter({
    history: createWebHistory(),
    routes: routes
})

//导出路由
export default router
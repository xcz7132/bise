package com.xcz.mapper;

import com.xcz.pojo.AfterSales;
import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface AfterSalesMapper {

    @Insert("insert into after_sales(car_id,client_id,information,employee_id," +
            "create_time,update_time) values(#{carId},#{clientId}," +
            "#{information},#{employeeId},#{createTime},#{updateTime})")
    void add(AfterSales sales);

    List<AfterSales> list(Integer carId, Integer clientId, Integer employeeId);

    @Update("update after_sales set car_id=#{carId},client_id=#{clientId},"+
            "information=#{information},employee_id=#{employeeId},update_time=#{updateTime} " +
            "where car_id=#{carId}")
    void update(AfterSales sales);
    @Delete("delete from car where after_sales_id=#{afterSalesId}")
    void delete(Integer id);

    @Select("select * from afterSales ")
    List<AfterSales> listAll();
}

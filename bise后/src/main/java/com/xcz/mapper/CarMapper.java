package com.xcz.mapper;

import com.xcz.pojo.Car;
import com.xcz.pojo.PageBean;
import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface CarMapper {

    List<Car> list(String state);

    @Insert("insert into car(car_name,content,price,state," +
            "create_time,update_time) values(#{carName},#{content}," +
            "#{price},#{state},#{createTime},#{updateTime})")
    void add(Car car);

    @Update("update car set car_name=#{carName},content=#{content},"+
            "price=#{price},state=#{state},update_time=#{updateTime} " +
            "where car_id=#{carId}")
    void update(Car car);

    @Delete("delete from car where car_id=#{id}")
    void delete(Integer id);

    @Select("select * from car ")
    List<Car> listAll();
}

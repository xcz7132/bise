package com.xcz.mapper;

import com.xcz.pojo.Trade;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Update;

import java.util.List;

@Mapper
public interface TradeMapper {
    @Insert("insert into trade(car_id,client_id,price,employee_id," +
            "create_time,update_time) values(#{carId},#{clientId}," +
            "#{price},#{employeeId},#{createTime},#{updateTime})")
    void add(Trade trade);

    List<Trade> list(Integer carId, Integer clientId, Integer employeeId);

    @Update("update trade set car_id=#{carId},client_id=#{clientId},"+
            "price=#{price},employee_id=#{employeeId},update_time=#{updateTime} " +
            "where order_id=#{orderId}")
    void update(Trade trade);

    @Delete("delete from trade where trade_id=#{tradeId}")
    void delete(Integer id);
}

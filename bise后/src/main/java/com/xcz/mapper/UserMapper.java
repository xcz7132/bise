package com.xcz.mapper;

import com.xcz.pojo.User;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import java.util.List;

@Mapper
public interface UserMapper {


    @Select("select * from user where username=#{username}")
    public User findByUserName(String username);

    @Insert("insert into user(username,password) values(#{username},#{password}) ")
    void add(String username, String password);

    @Update("update user set username=#{username} where id=#{id}")
    void update(User user);
    @Update("update user set password=#{md5String} where id =#{id}")
    void updatePwd(String md5String, Integer id);

    @Select("select * from user ")
    List<User> gtelist();
}

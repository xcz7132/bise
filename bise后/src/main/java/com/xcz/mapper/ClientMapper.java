package com.xcz.mapper;

import com.xcz.pojo.Client;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface ClientMapper {

    @Select("select * from client ")
    List<Client> list();
}

package com.xcz.utils;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class JsoupUntil{

    public static String jsoupP(String p) {
        // 解析HTML字符串
        Document document = Jsoup.parse(p);

        // 选择所有<p>标签
        Elements pTags = document.select("p");

        // 移除所有<p>标签，但保留其内容
        for (Element pTag : pTags) {
            pTag.unwrap();
        }

        // 获取处理后的HTML字符串
        String content = document.body().html();

        return content;
    }
}

package com.xcz.controller;

import com.xcz.pojo.*;
import com.xcz.service.ClientService;
import com.xcz.service.EmployeeService;
import com.xcz.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/employee")
public class EmployeeController {

    @Autowired
    private UserService userService;
    @Autowired
    private EmployeeService employeeService;

    @GetMapping("/list")
    public Result<PageBean<User>> list(Integer pageNum, Integer pageSize){
        PageBean<User> employeeList=userService.list(pageNum,pageSize);
        return Result.success(employeeList);
    }

    @GetMapping
    public Result<List<Employee>> list(){
        List<Employee> employeeList= employeeService.list();
        return Result.success(employeeList);
    }
}

package com.xcz.controller;

import com.xcz.anno.State;
import com.xcz.mapper.CarMapper;
import com.xcz.pojo.Car;
import com.xcz.pojo.Client;
import com.xcz.pojo.PageBean;
import com.xcz.pojo.Result;
import com.xcz.service.CarService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/car")
public class CarController {
    @Autowired
    private CarService carService;

    @PostMapping
    public Result add(@RequestBody @Validated Car car) {
        carService.add(car);
        return Result.success();
    }

    @GetMapping
    public Result<PageBean<Car>>list(Integer pageNum, Integer pageSize,
                                     @RequestParam(required = false) String state){
        PageBean<Car> pc= carService.list(pageNum,pageSize,state);
        return Result.success(pc);
    }
    @PutMapping
    public Result update(@RequestBody @Validated(Car.Update.class) Car car){
        carService.update(car);
        return Result.success();
    }
    @DeleteMapping
    public Result delete(Integer id){
        carService.delete(id);
        return Result.success();
    }


    @GetMapping("/list")
    public Result<List<Car>> list(){
        List<Car> carList= carService.listAll();
        return Result.success(carList);
    }
}

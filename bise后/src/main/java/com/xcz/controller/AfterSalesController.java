package com.xcz.controller;

import com.xcz.pojo.AfterSales;
import com.xcz.pojo.Car;
import com.xcz.pojo.PageBean;
import com.xcz.pojo.Result;
import com.xcz.service.AfterSalesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/afterSales")
public class AfterSalesController {
    @Autowired
    private AfterSalesService afterSalesService;

    @PostMapping
    public Result add(@RequestBody @Validated AfterSales sales) {
        afterSalesService.add(sales);
        return Result.success();
    }
    @GetMapping
    public Result<PageBean<AfterSales>>list(Integer pageNum, Integer pageSize,
                                       @RequestParam(required = false) Integer carId,
                                       @RequestParam(required = false) Integer clientId,
                                       @RequestParam(required = false) Integer employeeId){
        PageBean<AfterSales> po= afterSalesService.list(pageNum,pageSize,carId,clientId,employeeId);
        return Result.success(po);
    }
    @PutMapping
    public Result update(@RequestBody @Validated(AfterSales.Update.class) AfterSales sales){
        afterSalesService.update(sales);
        return Result.success();
    }
    @DeleteMapping
    public Result delete(Integer id){
        afterSalesService.delete(id);
        return Result.success();
    }

    @GetMapping("/list")
    public Result<List<AfterSales>> list(){
        List<AfterSales> afterSales= afterSalesService.listAll();
        return Result.success(afterSales);
    }
}

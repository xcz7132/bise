package com.xcz.controller;

import com.xcz.pojo.Car;
import com.xcz.pojo.Client;
import com.xcz.pojo.PageBean;
import com.xcz.pojo.Result;
import com.xcz.service.ClientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/client")
public class ClientController {
    @Autowired
    private ClientService clientService;

    @GetMapping
    public Result<List<Client>> list(){
        List<Client> clientList= clientService.list();
        return Result.success(clientList);
    }
}

package com.xcz.controller;

import com.xcz.pojo.Trade;
import com.xcz.pojo.PageBean;
import com.xcz.pojo.Result;
import com.xcz.service.TradeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/trade")
public class TradeController {
    @Autowired
    private TradeService orderService;

    @PostMapping
    public Result add(@RequestBody @Validated Trade trade) {
        orderService.add(trade);
        return Result.success();
    }
    @GetMapping
    public Result<PageBean<Trade>>list(Integer pageNum, Integer pageSize,
                                       @RequestParam(required = false) Integer carId,
                                       @RequestParam(required = false) Integer clientId,
                                       @RequestParam(required = false) Integer employeeId){
        PageBean<Trade> po= orderService.list(pageNum,pageSize,carId,clientId,employeeId);
        return Result.success(po);
    }
    @PutMapping
    public Result update(@RequestBody @Validated(Trade.Update.class) Trade trade){
        orderService.update(trade);
        return Result.success();
    }
    @DeleteMapping
    public Result delete(Integer id){
        try {
            orderService.delete(id);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        return Result.success();
    }
}

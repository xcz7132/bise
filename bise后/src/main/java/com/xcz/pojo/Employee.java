package com.xcz.pojo;

import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Pattern;
import lombok.Data;

@Data
public class Employee {

    private Integer employeeId;
    @NotEmpty//值不能为null,内容不能为空
    @Pattern(regexp = "^\\S{1,10}$")
    private String employeeName;
    private String phone;
}

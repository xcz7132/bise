package com.xcz.pojo;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.xcz.anno.State;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Pattern;
import jakarta.validation.groups.Default;
import lombok.Data;

import java.time.LocalDateTime;

@Data
public class Car {
    @NotNull(groups = Update.class)
    private  Integer carId;
    @NotEmpty//值不能为null,内容不能为空
    @Pattern(regexp = "^\\S{1,10}$")
    private String carName;
    private String content;
    private String price;
    @State
    private String state;
    @JsonFormat(pattern = "yyyy-MM-dd hh:mm:ss")
    private LocalDateTime createTime;
    @JsonFormat(pattern = "yyyy-MM-dd hh:mm:ss")
    private LocalDateTime updateTime;

    public interface Add extends Default {}
    public interface Update extends Default{}
}

package com.xcz.pojo;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Pattern;
import lombok.Data;

@Data
public class User {
    private Integer id;
    @NotEmpty//值不能为null,内容不能为空
    @Pattern(regexp = "^\\S{1,10}$")
    private String username;
    @JsonIgnore//让springmvc把当前对象转换成json字符串的时候，忽略password，最终的json字符串中就没有password这个属性了
    private String password;
    private String nickname;
    private String role;
}

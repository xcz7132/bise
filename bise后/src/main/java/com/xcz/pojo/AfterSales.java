package com.xcz.pojo;

import com.fasterxml.jackson.annotation.JsonFormat;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.groups.Default;
import lombok.Data;

import java.time.LocalDateTime;

@Data
public class AfterSales {
    @NotNull(groups = Update.class)
    private Integer afterSalesId;
    @NotNull
    private Integer carId;
    @NotNull
    private Integer clientId;
    private String information;
    @NotNull
    private Integer employeeId;
    @JsonFormat(pattern = "yyyy-MM-dd hh:mm:ss")
    private LocalDateTime createTime;
    @JsonFormat(pattern = "yyyy-MM-dd hh:mm:ss")
    private LocalDateTime updateTime;

    public interface Add extends Default {}
    public interface Update extends Default{}
}

package com.xcz.validation;

import com.xcz.anno.State;
import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;

public class StaterValidation implements ConstraintValidator<State/*给那个注解提供校验规则*/,
        String/*校验的数据类型*/>{
    /**
     *
     * @param value 将来要校验的数据
     * @param constraintValidatorContext
     * @return 如果返回false 校验不通过
     */
    @Override
    public boolean isValid(String value, ConstraintValidatorContext constraintValidatorContext) {
        //提供校验规则
        if (value==null){
            return false;
        }
        if (value.equals("已售出")||value.equals("未售出")){
            return true;
        }
        return false;
    }
}

package com.xcz;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BiSeApplication
{
    public static void main( String[] args )
    {
        SpringApplication.run(BiSeApplication.class,args);
        System.out.println("启动成功");
    }
}

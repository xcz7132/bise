package com.xcz.service;

import com.xcz.pojo.Employee;
import com.xcz.pojo.PageBean;
import com.xcz.pojo.User;

import java.util.List;

public interface UserService {
    //根据用户名查询
    User findByUserName(String username);

    //注册
    void register(String username, String username1);
    //更新
    void update(User user);
    //更新密码
    void updatePwd(String newPwd);

    //查询所有用户
    PageBean<User> list(Integer pageNum, Integer pageSize);
}

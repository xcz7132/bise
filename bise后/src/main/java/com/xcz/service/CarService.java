package com.xcz.service;

import com.xcz.pojo.Car;
import com.xcz.pojo.PageBean;

import java.util.List;

public interface CarService {
    //条件分页列表查询
    PageBean<Car> list(Integer pageNum, Integer pageSize, String state);
    //新增文章
    void add(Car car);
    //更新
    void update(Car car);
    //删除
    void delete(Integer id);

    List<Car> listAll();
}

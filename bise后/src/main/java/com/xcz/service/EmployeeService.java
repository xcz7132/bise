package com.xcz.service;

import com.xcz.pojo.Employee;

import java.util.List;

public interface EmployeeService {
    List<Employee> list();
}

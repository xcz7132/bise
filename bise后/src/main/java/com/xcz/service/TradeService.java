package com.xcz.service;

import com.xcz.pojo.Trade;
import com.xcz.pojo.PageBean;

public interface TradeService {
    void add(Trade trade);

    PageBean<Trade> list(Integer pageNum, Integer pageSize,
                         Integer carId, Integer clientId, Integer employeeId);

    void update(Trade trade);

    void delete(Integer id) throws Exception;
}

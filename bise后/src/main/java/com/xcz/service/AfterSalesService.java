package com.xcz.service;

import com.xcz.pojo.AfterSales;
import com.xcz.pojo.PageBean;

import java.util.List;

public interface AfterSalesService {
    void add(AfterSales sales);

    PageBean<AfterSales> list(Integer pageNum, Integer pageSize, Integer carId, Integer clientId, Integer employeeId);

    void update(AfterSales sales);

    void delete(Integer id);

    List<AfterSales> listAll();
}

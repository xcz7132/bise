package com.xcz.service;

import com.xcz.pojo.Car;
import com.xcz.pojo.Client;

import java.util.List;

public interface ClientService {
    List<Client> list();
}

package com.xcz.service.impl;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.xcz.mapper.CarMapper;
import com.xcz.pojo.Car;
import com.xcz.pojo.PageBean;
import com.xcz.service.CarService;
import com.xcz.utils.JsoupUntil;
import lombok.AllArgsConstructor;
import org.jsoup.Jsoup;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;

@Service
public class CarServiceImpl implements CarService {
    @Autowired
    private CarMapper carMapper;

    @Override
    public PageBean<Car> list(Integer pageNum, Integer pageSize, String state) {
        //创建PageBean对象
        PageBean<Car> pb=new PageBean<>();
        //开启分页查询 PageHelper
        PageHelper.startPage(pageNum,pageSize);
        //调用mapper
        List<Car> ls= carMapper.list(state);
        //Page中提供的方法，可以获取PageHelper分页查询后 得到的总记录条数和当前页数据
        Page<Car> p= (Page<Car>) ls;
        //把数据填充到PageBean对象中
        pb.setTotal(p.getTotal());
        pb.setItems(p.getResult());
        return pb;
    }

    @Override
    public void add(Car car) {
        car.setCreateTime(LocalDateTime.now());
        car.setUpdateTime(LocalDateTime.now());
        String newContent =JsoupUntil.jsoupP(car.getContent());
        car.setContent(newContent);
        carMapper.add(car);
    }

    @Override
    public void update(Car car) {
        car.setUpdateTime(LocalDateTime.now());
        carMapper.update(car);
    }

    @Override
    public void delete(Integer id) {
        carMapper.delete(id);
    }

    @Override
    public List<Car> listAll() {
        return carMapper.listAll();
    }


}

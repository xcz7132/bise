package com.xcz.service.impl;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.xcz.mapper.UserMapper;
import com.xcz.pojo.Car;
import com.xcz.pojo.Employee;
import com.xcz.pojo.PageBean;
import com.xcz.pojo.User;
import com.xcz.service.UserService;
import com.xcz.utils.Md5Util;
import com.xcz.utils.ThreadLocalUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class UserServiceImpl implements UserService {
    @Autowired
    private UserMapper userMapper;
    @Override
    public User findByUserName(String username) {
        User u = userMapper.findByUserName(username);
        return u;
    }
    @Override
    public void register(String username, String password) {
        //加密
        String md5String = Md5Util.getMD5String(password);
        //添加
        userMapper.add(username,md5String);
    }

    @Override
    public void update(User user) {
        userMapper.update(user);
    }

    @Override
    public void updatePwd(String newPwd) {
        String md5String = Md5Util.getMD5String(newPwd);
        Map<String,Object> map = ThreadLocalUtil.get();
        Integer id = (Integer) map.get("id");
        userMapper.updatePwd(md5String,id);
    }

    @Override
    public PageBean<User> list(Integer pageNum, Integer pageSize) {
        //创建PageBean对象
        PageBean<User> pu=new PageBean<>();
        //开启分页查询 PageHelper
        PageHelper.startPage(pageNum,pageSize);
        //调用mapper
        List<User> ls= userMapper.gtelist();
        //Page中提供的方法，可以获取PageHelper分页查询后 得到的总记录条数和当前页数据
        Page<User> p= (Page<User>) ls;
        //把数据填充到PageBean对象中
        pu.setTotal(p.getTotal());
        pu.setItems(p.getResult());
        return pu;
    }
}

package com.xcz.service.impl;

import com.xcz.mapper.ClientMapper;
import com.xcz.pojo.Car;
import com.xcz.pojo.Client;
import com.xcz.service.ClientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ClientServiceImpl implements ClientService {
    @Autowired
    private ClientMapper clientMapper;
    @Override
    public List<Client> list() {
        return clientMapper.list();
    }
}

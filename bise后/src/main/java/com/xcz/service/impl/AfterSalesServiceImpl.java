package com.xcz.service.impl;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.xcz.mapper.AfterSalesMapper;
import com.xcz.pojo.AfterSales;
import com.xcz.pojo.PageBean;
import com.xcz.service.AfterSalesService;
import com.xcz.utils.JsoupUntil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;

@Service
public class AfterSalesServiceImpl implements AfterSalesService {
    @Autowired
    private AfterSalesMapper afterSalesMapper;
    @Override
    public void add(AfterSales sales) {
        sales.setCreateTime(LocalDateTime.now());
        sales.setUpdateTime(LocalDateTime.now());
        String newInformation = JsoupUntil.jsoupP(sales.getInformation());
        sales.setInformation(newInformation);
        afterSalesMapper.add(sales);
    }

    @Override
    public PageBean<AfterSales> list(Integer pageNum, Integer pageSize, Integer carId, Integer clientId, Integer employeeId) {
        //创建PageBean对象
        PageBean<AfterSales> pb=new PageBean<>();
        //开启分页查询 PageHelper
        PageHelper.startPage(pageNum,pageSize);
        //调用mapper
        List<AfterSales> las= afterSalesMapper.list(carId,clientId,employeeId);
        //Page中提供的方法，可以获取PageHelper分页查询后 得到的总记录条数和当前页数据
        Page<AfterSales> p= (Page<AfterSales>) las;
        //把数据填充到PageBean对象中
        pb.setTotal(p.getTotal());
        pb.setItems(p.getResult());
        return pb;
    }

    @Override
    public void update(AfterSales sales) {
        sales.setUpdateTime(LocalDateTime.now());
        afterSalesMapper.update(sales);
    }

    @Override
    public void delete(Integer id) {
        afterSalesMapper.delete(id);
    }

    @Override
    public List<AfterSales> listAll() {
        return afterSalesMapper.listAll();
    }
}

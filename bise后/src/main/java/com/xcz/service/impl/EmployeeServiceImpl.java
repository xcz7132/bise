package com.xcz.service.impl;

import com.xcz.mapper.EmployeeMapper;
import com.xcz.pojo.Employee;
import com.xcz.service.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EmployeeServiceImpl implements EmployeeService {
    @Autowired
    private EmployeeMapper employeeMapper;
    @Override
    public List<Employee> list() {
        return employeeMapper.list();
    }
}

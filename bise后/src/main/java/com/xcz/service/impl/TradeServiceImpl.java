package com.xcz.service.impl;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.xcz.mapper.TradeMapper;
import com.xcz.mapper.UserMapper;
import com.xcz.pojo.Trade;
import com.xcz.pojo.PageBean;
import com.xcz.pojo.User;
import com.xcz.service.TradeService;
import com.xcz.utils.ThreadLocalUtil;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.lang.management.GarbageCollectorMXBean;
import java.lang.management.ThreadInfo;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;

@Service
public class TradeServiceImpl implements TradeService {
    @Autowired
    private TradeMapper tradeMapper;

    @Autowired
    private UserMapper userMapper;
    @Override
    public void add(Trade trade) {
        trade.setUpdateTime(LocalDateTime.now());
        trade.setCreateTime(LocalDateTime.now());
        tradeMapper.add(trade);
    }

    @Override
    public PageBean<Trade> list(Integer pageNum, Integer pageSize, Integer carId, Integer clientId, Integer employeeId) {
        Map<String, Object> claims= ThreadLocalUtil.get();
        String username = (String) claims.get("username");
        User user = userMapper.findByUserName(username);
        if (user.getRole().equals("销售")){
            employeeId=user.getId();
        }
        //创建PageBean对象
        PageBean<Trade> pb=new PageBean<>();
        //开启分页查询 PageHelper
        PageHelper.startPage(pageNum,pageSize);
        //调用mapper
        List<Trade> lo= tradeMapper.list(carId,clientId,employeeId);
        //Page中提供的方法，可以获取PageHelper分页查询后 得到的总记录条数和当前页数据
        Page<Trade> p= (Page<Trade>) lo;
        //把数据填充到PageBean对象中
        pb.setTotal(p.getTotal());
        pb.setItems(p.getResult());
        return pb;
    }

    @Override
    public void update(Trade trade) {
        Map<String, Object> claims= ThreadLocalUtil.get();
        String username = (String) claims.get("username");
        User user = userMapper.findByUserName(username);
        if (user.getRole().equals("销售")){
            trade.setEmployeeId(user.getId());
        }
        trade.setUpdateTime(LocalDateTime.now());
        tradeMapper.update(trade);
    }

    @Override
    public void delete(Integer id) throws Exception {
        Map<String, Object> claims= ThreadLocalUtil.get();
        String username = (String) claims.get("username");
        User user = userMapper.findByUserName(username);
        if (user.getRole().equals("销售")) {
            tradeMapper.delete(id);
        }
        throw new Exception("权限不足");
    }
}

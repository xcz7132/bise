/*
SQLyog Community v13.1.6 (64 bit)
MySQL - 8.0.36 : Database - bise
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`bise` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci */ /*!80016 DEFAULT ENCRYPTION='N' */;

USE `bise`;

/*Table structure for table `after_sales` */

DROP TABLE IF EXISTS `after_sales`;

CREATE TABLE `after_sales` (
  `after_sales_id` int unsigned NOT NULL AUTO_INCREMENT COMMENT '售后id',
  `car_id` int unsigned NOT NULL COMMENT '车辆id',
  `client_id` int unsigned NOT NULL COMMENT '客户id',
  `information` varchar(1000) DEFAULT NULL COMMENT '信息',
  `employee_id` int unsigned NOT NULL COMMENT '员工id',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `update_time` datetime NOT NULL COMMENT '修改时间',
  PRIMARY KEY (`after_sales_id`),
  KEY `fk_after_sales_car` (`car_id`),
  KEY `fk_after_sales_client` (`client_id`),
  KEY `fk_after_sales_employee` (`employee_id`),
  CONSTRAINT `fk_after_sales_car` FOREIGN KEY (`car_id`) REFERENCES `car` (`car_id`),
  CONSTRAINT `fk_after_sales_client` FOREIGN KEY (`client_id`) REFERENCES `client` (`client_id`),
  CONSTRAINT `fk_after_sales_employee` FOREIGN KEY (`employee_id`) REFERENCES `employee` (`employee_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

/*Data for the table `after_sales` */

insert  into `after_sales`(`after_sales_id`,`car_id`,`client_id`,`information`,`employee_id`,`create_time`,`update_time`) values 
(1,1,1,'发动机损坏',1,'2024-03-08 16:13:59','2024-03-08 16:13:59'),
(2,6,1,'车辆故障',2,'2024-03-09 12:25:43','2024-03-09 12:25:49');

/*Table structure for table `car` */

DROP TABLE IF EXISTS `car`;

CREATE TABLE `car` (
  `car_id` int unsigned NOT NULL AUTO_INCREMENT COMMENT '车辆id',
  `car_name` varchar(20) DEFAULT NULL COMMENT '车辆名',
  `content` varchar(10000) DEFAULT NULL COMMENT '车辆信息',
  `price` varchar(128) DEFAULT NULL COMMENT '价格',
  `state` varchar(3) DEFAULT '未售出' COMMENT '状态(是否售出)',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `update_time` datetime NOT NULL COMMENT '修改时间',
  PRIMARY KEY (`car_id`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

/*Data for the table `car` */

insert  into `car`(`car_id`,`car_name`,`content`,`price`,`state`,`create_time`,`update_time`) values 
(1,'比亚迪01','比亚迪','15316','已售出','2024-03-08 10:52:26','2024-03-08 15:23:42'),
(6,'比亚迪02','比亚迪','15316','未售出','2024-03-08 19:07:11','2024-03-08 19:07:11'),
(8,'比亚迪03','比亚迪','15316','未售出','2024-03-08 19:07:14','2024-03-08 19:07:14'),
(20,'宝马004','宝马004','','未售出','2024-03-09 10:41:47','2024-03-09 10:41:47'),
(21,'宝马005','宝马004','','未售出','2024-03-09 10:43:44','2024-03-09 10:43:44');

/*Table structure for table `client` */

DROP TABLE IF EXISTS `client`;

CREATE TABLE `client` (
  `client_id` int unsigned NOT NULL AUTO_INCREMENT COMMENT '客户Id',
  `client_name` varchar(20) NOT NULL COMMENT '客户名',
  `password` varchar(32) DEFAULT NULL COMMENT '密码',
  `email` varchar(128) DEFAULT '' COMMENT '邮件',
  `phone` varchar(128) DEFAULT '' COMMENT '手机号',
  PRIMARY KEY (`client_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

/*Data for the table `client` */

insert  into `client`(`client_id`,`client_name`,`password`,`email`,`phone`) values 
(1,'aaaaa','123456','aa@123.com','14725836923');

/*Table structure for table `employee` */

DROP TABLE IF EXISTS `employee`;

CREATE TABLE `employee` (
  `employee_id` int unsigned NOT NULL AUTO_INCREMENT COMMENT '员工id',
  `employee_name` varchar(20) DEFAULT NULL COMMENT '员工名',
  `phone` varchar(128) DEFAULT '' COMMENT '联系方式',
  PRIMARY KEY (`employee_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

/*Data for the table `employee` */

insert  into `employee`(`employee_id`,`employee_name`,`phone`) values 
(1,'张三','13111111111'),
(2,'李四','14555345684');

/*Table structure for table `trade` */

DROP TABLE IF EXISTS `trade`;

CREATE TABLE `trade` (
  `trade_id` int unsigned NOT NULL AUTO_INCREMENT COMMENT '订单id',
  `car_id` int unsigned NOT NULL COMMENT '车辆id',
  `client_id` int unsigned NOT NULL COMMENT '客户id',
  `price` varchar(128) DEFAULT NULL COMMENT '成交价格',
  `employee_id` int unsigned NOT NULL COMMENT '员工id',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `update_time` datetime NOT NULL COMMENT '修改时间',
  PRIMARY KEY (`trade_id`),
  KEY `fk_order_car` (`car_id`),
  KEY `fk_order_client` (`client_id`),
  KEY `fk_order_employee` (`employee_id`),
  CONSTRAINT `fk_order_car` FOREIGN KEY (`car_id`) REFERENCES `car` (`car_id`),
  CONSTRAINT `fk_order_client` FOREIGN KEY (`client_id`) REFERENCES `client` (`client_id`),
  CONSTRAINT `fk_order_employee` FOREIGN KEY (`employee_id`) REFERENCES `employee` (`employee_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

/*Data for the table `trade` */

insert  into `trade`(`trade_id`,`car_id`,`client_id`,`price`,`employee_id`,`create_time`,`update_time`) values 
(3,1,1,'123456',1,'2024-03-08 16:11:02','2024-03-08 16:11:02');

/*Table structure for table `user` */

DROP TABLE IF EXISTS `user`;

CREATE TABLE `user` (
  `id` int unsigned NOT NULL AUTO_INCREMENT COMMENT 'id',
  `username` varchar(20) DEFAULT NULL COMMENT '用户名',
  `password` varchar(32) DEFAULT NULL COMMENT '密码',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

/*Data for the table `user` */

insert  into `user`(`id`,`username`,`password`) values 
(1,'123','e10adc3949ba59abbe56e057f20f883e'),
(2,'123456','e10adc3949ba59abbe56e057f20f883e');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
